# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['Expiration']



class Expiration(ModelSQL, ModelView):
    "Document Expiration"
    __name__ = "document.expiration"
    _rec_name = 'number'
    kind = fields.Many2One('document.type_document', 'Kind',
            required=True)
    date_start = fields.Date('Date Start')
    date_end = fields.Date('Date End')
    description = fields.Text("Description")
    days_alert = fields.Integer('Days Alert')

    @classmethod
    def __setup__(cls):
        super(Expiration, cls).__setup__()
